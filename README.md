# MovieGen Predictor

![nombreDeLaFoto](banner_proyecto.jpg)

## Autores

Juan David Contreras - 2201432, Fandry Lizeth Bernal - 2205619, Luna Valentina Gaona - 2215504

## Objetivo

Predecir con la mayor exactitud posible el género de una película a partir de su título y año de salida.

## Conjunto de datos

El dataset utilizado es el resultado de una limpieza de datos de MovieLens, un dataset que recopila información interesante de una cantidad considerable de películas de todos los años y géneros. Se encuentra disponible para descargar en el siguiente enlace: https://grouplens.org/datasets/movielens/

## Modelos

- Decision Tree Classifier
- Random Forest Classifier
- Support Vector Classifier (SVC)

## Enlaces

- Código: https://colab.research.google.com/drive/1YQ1vWu_N7LnXv9bCutnGK-WDLn7jOmYA?usp=sharing
- Video: https://youtu.be/NIO981zKNpE
- Repositorio: https://gitlab.com/movies_ia/moviegen_predictor

